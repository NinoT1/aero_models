% WHY IS THIS FILE IN BROKEN?
% Output results are incorrect for all but a few aerodynamic
% configurations. A1 is at least 100x too large, and even numbered A values
% are not nearly zero like they should be. Something with the setup of the
% RHS matrix is incorrect with this implementation. Possibly with the chord
% interpolation function I wrote? But even that doesn't fix it. Beats me.
% This particular implementration is found in several textbooks where
% various aerodynamic parameters are substituted with a "mu" variable. I am
% not a fan of this implementation because I have not had much success with
% writing a script for it. Use this function at your own risk!
% --Nino Tarantino

% ---------------------------------------------------------------------- %
% PROGRAMMER:  Nino Tarantino                                            %
% DATE:        August 5, 2017                                            %
% REFERENCES:  Theory of Wing Sections (Abbott, von Doenhoff)            %
%              Aerodynamics for Engineering Students (Houghton)          %
% ASSUMPTIONS: No twist, no spanwise variation of lambda                 %
% ---------------------------------------------------------------------- %

function [A] = ...
    prandtl_improved(N, b, c_r, c_t, a_r, a_t, i_r, i_t)

    RHS = zeros(N,N);
    LHS = zeros(N,1);
    
    % wing geometry
    spacing = (pi/2)/N;  % spacing between theta values
    theta   = linspace(spacing, pi/2, N);
    c       = c_r * (1 + (c_r-c_t)/c_r * cos(theta)); % spanwise chord variation
    a       = a_r * (1 - (a_r-a_t)/a_r * cos(theta)); % spandwise 2-d lift slope variation
    alpha   = i_r * (1 + (i_r-i_t)/i_r * cos(theta)); % spanwise absolute incidence variation
    mu      = c.*a/(8*b/2);
    
    % calculations
    LHS = mu.*alpha.*sin(theta);
    n = linspace(1, 2*N-1, N);
    for i = 1:1:N
        for n = 1:2:2*N-1
            RHS(i,(n+1)/2) = sin(n*theta(i)) * (1+mu(i)*n/sin(theta(i)));
        end
    end
    A = RHS\(LHS')
end