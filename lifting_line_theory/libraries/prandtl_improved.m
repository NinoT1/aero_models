%PRANDTL_IMPROVED implements Prandtl's Lifting Line Theory for a wing with
%given geometric properties. Spanwise variation in chord, angle of attack,
%and section zero-lift angle of attack is supported.
%   PRANDTL_IMPROVED(N, b, c_r, c_t, cl_r, cl_t, alpha0_r, alpha0_t, twist, AOA)
%   returns the coefficient of lift, coefficient of induced drag, and span
%   efficiency factor for the given wing. This is a more involved Lifting
%   Line code, taking into account a larger variety of wing shapes.
%
% Inputs:
%   N:        number of spanwise stations
%   c_r:      wing root chord
%   c_t:      wing tip chord
%   cl_r:     root sectional lift curve slope
%   cl_t:     tip sectional lift curve slope
%   alpha0_r: root zero-lift angle of attack (DEGREES)
%   alpha0_t: tip zero-lift angle of attack (DEGREES)
%   twist:    linear wing twist (DEGREES)
%   AOA:      aircraft angle of attack (DEGREES)
%
% Outputs:
%   CL:     3-d lift coefficient
%   CDi:    induced drag coefficient
%   e:      span efficiency factor
%
% See also PRANDTL, NACA_TAT

% ---------------------------------------------------------------------- %
% PROGRAMMER:  Nino Tarantino                                            %
% DATE:        August 12, 2017                                           %
% REFERENCES:  Theory of Wing Sections (Abbott, von Doenhoff)            %
%              Aerodynamics for Engineering Students (Houghton)          %
% ASSUMPTIONS: No wing sweep                                             %
% ---------------------------------------------------------------------- %

function [CL, CDi, e] = ...
    prandtl_improved(N, b, c_r, c_t, cl_r, cl_t, alpha0_r, alpha0_t, twist, AOA)
    
%---- wing geometry calculations ----%
theta  = linspace(0, pi, N); % wing stations in theta coordinates
y      = -1/2*b*cos(theta);  % y-theta transform
S      = 1/2*b*(c_r+c_t);    % wing area
c      = interp1([-b/2,0,b/2], [c_t,c_r,c_t], y); % chord at each station
AR     = b^2/S;              % aspect ratio


%---- aerodynamic inputs refined ----%
% 2-d section lift slope at each station (1/rad)
cl     = interp1([-b/2,0,b/2], [cl_t, cl_r, cl_t], y);
% zero-lift AOA at each station (radians)
alpha0 = interp1([-b/2,0,b/2], [alpha0_t, alpha0_r, alpha0_t], y)' * pi/180;
% angle of attack at each station (radians)
alpha  = (AOA-twist)*ones(N,1) * pi/180;


%---- solving the matrix equation ----%
% note: for this code I've defined the side of the equation with alpha
% as the left-hand side and the side of the equation with the A_n
% values as the right-hand-side.
LHS = alpha(2:N-1)-alpha0(2:N-1);

RHS = zeros(N-2, N-2); % preallocation
n = 1:N-2;
for i=2:N-1  % rows of the RHS matrix
   RHS(i-1,:) = sin(n*theta(i)) .* (4*b/(cl(i)*c(i)) + n./sin(theta(i)));
end

% solve the matrix equation for A_n values.
% NOTE: most aerodynamic configurations should have A(1)~= 0.01-0.05.
% If your A(1) value is negative or abnormally large, you may want to
% recheck your inputted wing geometry! If everything is correct, well
% it looks like your wing produces negative lift. Whoops.
A = RHS\LHS;


%---- aerodynamic data calculations ----%
CL    = A(1)*pi*AR;
delta = sum((n').*(A/A(1)).^2) - 1;
CDi   = CL^2/(pi*AR) * (1+delta);
e     = 1/(1+delta);

end