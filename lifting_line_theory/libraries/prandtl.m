%PRANDTL Implements Prandtl's Lifting Line Theory for a wing with a given
%aspect ratio, sweep, and linear twist.
%   PRANDTL(N, a, AR, lambda, alpha, alpha0, twist) returns the coefficient
%   of lift, coefficient of induced drag, and span efficiency factor for the
%   given wing.
%   This function is intended for quick, rough estimates. For a more
%   complete and accurate Lifting Line Implementation, see
%   prandtl_improved.
%
% Inputs:
%   N:      number of spanwise stations
%   a:      section lift-curve slope
%   AR:     aspect ratio
%   lambda: taper ratio (ct/cr)
%   alpha:  angle of attack
%   alpha0: sectional zero-lift angle of attack
%   twist:  linear twist (if applicable)
%
% Outputs:
%   CL:     3-d lift coefficient
%   CDi:    induced drag coefficient
%   e:      span efficiency factor
%
% See also PRANDTL_IMPROVED, NACA_TAT

% ---------------------------------------------------------------------- %
% PROGRAMMER:  Nino Tarantino                                            %
% DATE:        August 5, 2017                                            %
% REFERENCES:  Theory of Wing Sections (Abbott, von Doenhoff)            %
%              Aerodynamics for Engineering Students (Houghton)          %
% ASSUMPTIONS: No spanwise variation in airfoil, no sweep                %
% ---------------------------------------------------------------------- %

function [CL, CDi, e] = prandtl(N, a, AR, lambda, alpha, alpha0, twist)

%---- initialize matrices ----%
RHS   = zeros(1,N);
LHS   = zeros(N,N);


%---- calculate wing geometry ----%
n     = 1:N;
theta = n*pi/(2*N);

%nondimensional collection of wing geometry terms (see Houghton)
mu    = (a / (2*AR*(1+lambda))) * (1 + (lambda-1)*cos(theta));


%---- solve the matrix equation ----%
RHS = mu .* sin(theta) * (alpha-alpha0+twist);

for n = 1:2:2*N-1  % iterating through rows on the LHS
    LHS((n+1)/2,:) = sin(n*theta) .* (n*mu+sin(theta));
end

A = LHS\(RHS');


%---- calculate function outputs ----%
CL    = A(1)*pi*AR;
delta = sum((n').*(A/A(1)).^2) - 1;
CDi   = CL^2 / (pi*AR) * (1+delta);
e     = 1/(1+delta);

end