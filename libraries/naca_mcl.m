%NACA_MCL NACA Mean Camber Line generates an analytical equation for the
%mean camber line of a NACA 4-digit, 5-digit, or 6-series airfoil.
%
%    NACA_MCL returns two piecewise equations and a breakpoint; the
%    breakpoint is the percentage of the chord where the transition between
%    the two equations occurs.
%
%    AIRFOIL should be entered as a character array without dashes, spaces,
%    or "NACA" preceding it. The user should note that with the 6-series
%    airfoil the breakpoint will be the the point where the mean camber
%    line equation will evaluate to NaN. The user will need to eliminate
%    this point when plotting to get an accurate plot. When evaluating a
%    6-series airfoil, do not include the subscript below the second number
%    in the designation.
%
%    example: [z1, z2, breakpoint] = naca_mcl('2412')
%
%    example: [z1, z2, breakpoint] = naca_mcl('62315', 0.6)
%
% Inputs:
%   AIRFOIL:    the NACA airfoil designation as a character array
%   A:          percentage of airfoil chord over which pressure
%               distribution is uniform (6-series only)
%
% Outputs:
%   z1:         nondimensionalized camber line equation 1
%   z2:         nondimensionalized camber line equation 2
%   breakpoint: location where z1 transitions to z2

% ---------------------------------------------------------------------- %
% PROGRAMMER:  Nino Tarantino                                            %
% DATE:        August 15, 2017                                           %
% REFERENCES:  Fundamentals of Aerodynamics (Anderson)                   %
% ASSUMPTIONS: NACA 4-digit and 5-digit airfoils (including 6-series)    %
% ---------------------------------------------------------------------- %

function [z1, z2, breakpoint] = naca_mcl(AIRFOIL, A)

%---- parsing user input and error handling ----%
switch ischar(AIRFOIL)
    % we want the input to be a character array
    case 1
        % do nothing
    case 0
        AIRFOIL = char(AIRFOIL);
end
if numel(AIRFOIL)<4 || numel(AIRFOIL)>5
    if contains(AIRFOIL, "NACA")
        % we just need the designation, we already know it's a NACA airfoil
        warning('do not include "NACA" in airfoil designation');
        AIRFOIL(strfind(AIRFOIL, ' ')) = []; % assuming there's a space too
        AIRFOIL = AIRFOIL(5:end);
    elseif contains(AIRFOIL, " ")
        warning('do not include spaces in airfoil designation');
        AIRFOIL(strfind(AIRFOIL, ' ')) = [];
    else
        error('provided airfoil designation is invalid')
    end
end

% x stands for x/c ratio
syms x


%---- NACA 4-digit airfoil ----%
if numel(AIRFOIL) == 4
    m = str2double(AIRFOIL(1))/100; % max camber
    p = str2double(AIRFOIL(2))/10;  % location of max camber
    
    if p==0 % symmetric airfoil
        z1 = 0;
        z2 = 0;
    else % cambered airfoil
        z1  = m/p^2 * (2*p*x-x^2);                 % from x=0 to x=p
        z2  = m/(1-p)^2 * ((1-2*p) + 2*p*x - x^2); % from x=p to x=1
    end

    breakpoint = p;


%---- NACA 5-digit airfoil ----%
elseif (numel(AIRFOIL) == 5) && (AIRFOIL(1) ~= 6)
    cl = str2double(AIRFOIL(1))*(3/2)/10; % design lift coefficient
    p  = str2double(AIRFOIL(2:3))/2/100;  % location of max camber

    m  = fzero(@(m) m*(1-sqrt(m/3))-p, [0 1]);
    q  = (3*m-7*m^2+8*m^3-4*m^4)/sqrt(m-m^2) - 1.5*(1-2*m)*(pi/2-asin(1-2*m));
    k1 = 6*cl/q;

    z1 = k1/6 * (x^3-3*m*x^2 + m^2*(3-m)*x); % from x=0 to x=m
    z2 = k1*m^3/6 * (1-x);                   % from x=m to x=1

    breakpoint = m;


%---- NACA 6-series airfoil ----%
% this is only a close approximation, since 6-series airfoils are 
% designed by conformal mapping procedures rather than geometric
% relationships
elseif (numel(AIRFOIL) == 5) && (AIRFOIL(1) == 6)
    % if no A value or invalid value specified, default to 0
    if ~exist('A','var') || A==1
        A = 0;
    end

    cl = str2double(AIRFOIL(3))/10; % design lift coefficient

    g = -1/(1-A) * (A^2*(1/2*log(A)-1/4) + 1/4);
    h = 1/(1-A) * (1/2*(1-A)^2*log(1-A)-1/4*(1-A)^2) + g;

    % in this case, only a single function is used to estimate the mcl
    z1 = cl/(2*pi*(A+1)) * (1/(1-A)*(1/2*(A-x)^2*log2(abs(A-x)) ...
         -1/2*(1-x)^2*log(1-x) + 1/4*(1-x)^2 - 1/4*(A-x)^2) ...
         -x*log(x) + g - h*x);
    z2 = z1;
    breakpoint = A; % note that this point will evaluate to NaN!


%---- Unsupported airfoil or incorrect input ----%
else
    error('invalid airfoil designation')
end

end