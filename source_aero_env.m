%SOURCE_AERO_ENV Source Aero Environment adds all subdirectories of this
%git repo to the MATLAB search path. Insert at the start of a function for
%to quickly include all functions from the aero_model repo in your search
%path.
%
% Inputs:
%   None
%
% Outputs:
%   None

% ---------------------------------------------------------------------- %
% PROGRAMMER:  Nino Tarantino                                            %
% DATE:        September 23, 2017                                        %
% REFERENCES:  None                                                      %
% ASSUMPTIONS: None                                                      %
% ---------------------------------------------------------------------- %

function [] = source_aero_env()

switch isunix()
    case 1 % unix environment
        dir_separator = '/';
    case 0 % windows environment
        dir_separator = '\';
end

% add the entire aero_models repo to the search path
addpath(genpath(pwd));

% remove all git artifacts
rmpath(genpath(strcat(pwd, dir_separator, '.git')));

end