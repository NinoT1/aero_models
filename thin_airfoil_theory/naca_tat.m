%NACA_TAT NACA Thin Airfoil Theory calculates aerodynamic characteristics
%for a NACA airfoil at a given angle of attack.
%
%    AIRFOIL should be entered as a character array without dashes, spaces,
%    or "NACA" preceding it. The user should note that with the 6-series
%    airfoil the breakpoint will be the the point where the mean camber
%    line equation will evaluate to NaN. The user will need to eliminate
%    this point when plotting to get an accurate plot.
%
% Inputs:
%   AIRFOIL:    the NACA airfoil designation as a character array
%   alpha:      angle of attack (DEGREES)
%   A:          percentage of airfoil chord over which pressure
%               distribution is uniform (6-series only)
%
% Outputs:
%   alpha0:     zero-lift angle of attack (DEGREES)
%   cl:         section lift coefficient
%   cm_quarter: moment coefficient at the quarter chord
%   x_cp:       center of pressure, nondimensionalized by chord
%
% See also NACA_MCL, PRANDTL, PRANDTL_IMPROVED

% ---------------------------------------------------------------------- %
% PROGRAMMER:  Nino Tarantino                                            %
% DATE:        August 19, 2017                                           %
% REFERENCES:  Fundamentals of Aerodynamics (Anderson)                   %
% ASSUMPTIONS: Low angles of attack; inviscid, incompressible flow       %
%              Thickness around 12% or lower                             %
% ---------------------------------------------------------------------- %

function [alpha0, cl, cm_quarter, x_cp] = naca_tat(AIRFOIL, alpha, A)

%---- error checking and search path modification ----%
if exist('A','var') && A==1
    A = 0; % default to zero, A cannot be 1.0
end

% add the general functions directory to the search path
switch isunix()
    case 1 % unix environment
        dir_separator = '/';
    case 0 % windows environment
        dir_separator = '\';
end
addpath(strcat('..',dir_separator,'libraries')); % needed for naca_mcl


%---- implement thin-airfoil theory for cambered airfoils ----%
% convert alpha to radians
alpha = deg2rad(alpha);

% get mean camber line equation
syms x
if exist('A','var') % NACA 6-series airfoil
    [z1, z2, mcl_breakpoint] = naca_mcl(AIRFOIL,A);
else % other airfoil
    [z1, z2, mcl_breakpoint] = naca_mcl(AIRFOIL);
end

% get derivatives of the mcl
dz1 = diff(z1, x);
dz2 = diff(z2, x);

% transform from x-coordinates to theta-coordinates
syms theta
dz1 = subs(dz1, x, 1/2*(1-cos(theta)));
dz2 = subs(dz2, x, 1/2*(1-cos(theta)));
mcl_breakpoint = acos(1-2*mcl_breakpoint);

% calculate the zero-lift angle of attack
alpha0 = -1/pi * (int(dz1*(cos(theta)-1),0,mcl_breakpoint) + ...
                  int(dz2*(cos(theta)-1),mcl_breakpoint,pi));

% calculate section lift coefficient
cl = 2*pi*(alpha + 1/pi*(int(dz1*(cos(theta)-1),0,mcl_breakpoint) + ...
                         int(dz2*(cos(theta)-1),mcl_breakpoint,pi)));

% calculate moment coefficient at the quarter chord
% first we will need the first two Fourier coefficients
A1 = 2/pi*(int(dz1*cos(theta),0,mcl_breakpoint)+int(dz2*cos(theta),mcl_breakpoint,pi));
A2 = 2/pi*(int(dz1*cos(2*theta),0,mcl_breakpoint)+int(dz2*cos(2*theta),mcl_breakpoint,pi));
cm_quarter = pi/4*(A2-A1);
% It should be noted that the moment coefficient calculated from thin
% airfoil theory states that the quarter chord is the aerodynamic center.
% This assumption is a reasonable estimate for many conventional airfoils,
% but for some applications this estimate will not be reasonable.

% calculate center of pressure
x_cp = 1/4*(1 + pi/cl*(A1-A2));


%---- convert all quantities to double precision from symbolic ----%
alpha0     = rad2deg(double(alpha0));
cl         = double(cl);
cm_quarter = double(cm_quarter);
x_cp       = double(x_cp);

end